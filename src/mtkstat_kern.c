#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <arpa/inet.h>
#include "bpf_helpers.h"
#include "mtkstat_common.h"

struct bpf_map_def SEC("maps") filters_map = {
	.type		= BPF_MAP_TYPE_PERCPU_ARRAY,
	.key_size	= sizeof(__u32),
	.value_size	= sizeof(struct filter_data),
	.max_entries	= FILTERS_MAX,
};

struct pkt_info {
	__u32 src_addr;
	__u32 dst_addr;
	__u16 src_port;
	__u16 dst_port;
	__u8  protocol;
	__u16 size;
};

static __always_inline
int mtk_filter_pkt(__u32 n_filter, struct pkt_info pkt)
{
	if (n_filter >= FILTERS_MAX)
		return -1;

	struct filter_data *filter = bpf_map_lookup_elem(&filters_map,
							 &n_filter);

	if (!filter)
		return -1;

	if (!filter->enabled)
		return -1;

	if ((filter->src_addr == pkt.src_addr || filter->src_addr == 0) &&
	    (filter->src_port == pkt.src_port || filter->src_port == 0) &&
	    (filter->dst_addr == pkt.dst_addr || filter->dst_addr == 0) &&
	    (filter->dst_port == pkt.dst_port || filter->dst_port == 0) &&
	    (filter->protocol == pkt.protocol || filter->protocol == 0)) {
		filter->rx_pkts++;
		filter->rx_bytes += pkt.size;
	}

	return 0;
}

static __always_inline
int mtk_parse_hdrs(void *data, void *data_end, struct pkt_info *pkt)
{
	struct ethhdr *eth = data;
	void *hdr_ptr = data + sizeof(*eth);

	if (hdr_ptr > data_end)
		return -1;

	if (eth->h_proto != htons(ETH_P_IP))
		return -1;

	struct iphdr *iph = hdr_ptr;

	if (hdr_ptr + sizeof(*iph) > data_end)
		return -1;

	hdr_ptr += sizeof(*iph);

	if (iph->protocol == IPPROTO_UDP) {
		struct udphdr *udp = hdr_ptr;

		if (hdr_ptr + sizeof(*udp) > data_end)
			return -1;

		pkt->src_port = udp->source;
		pkt->dst_port = udp->dest;
	} else if (iph->protocol == IPPROTO_TCP) {
		struct tcphdr *tcp = hdr_ptr;

		if (hdr_ptr + sizeof(*tcp) > data_end)
			return -1;

		pkt->src_port = tcp->source;
		pkt->dst_port = tcp->dest;
	} else
		return -1;

	pkt->src_addr = iph->saddr;
	pkt->dst_addr = iph->daddr;
	pkt->protocol = iph->protocol;
	pkt->size = data_end - data;

	return 0;
}

SEC("xdp_prog")
int xdp_pass(struct xdp_md *ctx)
{
	void *data = (void *)(long)ctx->data;
	void *data_end = (void *)(long)ctx->data_end;
	struct pkt_info pkt;
	int err;

	err = mtk_parse_hdrs(data, data_end, &pkt);
	if (err)
		return XDP_PASS;

	/* hardcode unrolled loop since loops are forbidden :( */
	err = mtk_filter_pkt(0, pkt);
	if (err)
		return XDP_PASS;

	err = mtk_filter_pkt(1, pkt);
	if (err)
		return XDP_PASS;

	err = mtk_filter_pkt(2, pkt);
	if (err)
		return XDP_PASS;

	err = mtk_filter_pkt(3, pkt);
	if (err)
		return XDP_PASS;

	err = mtk_filter_pkt(4, pkt);
	if (err)
		return XDP_PASS;

	return XDP_PASS;
}

char _license[] SEC("license") = "GPL";
