#include <zmq.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>

static void *ctx;
static void *socket;
static char endpoint[32] = "tcp://localhost:";
static char filter[2];

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

static void zcli_exit(int status)
{
	zmq_close(socket);
	zmq_ctx_destroy(ctx);

	exit(status);
}

static void zcli_exit_sig(int sig)
{
	zcli_exit(0);
}

static void zcli_print_usage(char *prog_name)
{
	printf("%s [-p <port_number>] [-n <filter_index>]\n", prog_name);
	printf("\t-p <port_num>\tPort number from which read ZeroMQ messages.\n"
	       "\t\t\tOptional, default is 5555\n");
	printf("\t-n <filter_idx>\tPrint statistics only for specified filter.\n"
	       "\t\t\tOptional, if not set, print all.\n");
}

static int zcli_parse_command_line(int argc, char **argv)
{
	int opt;
	bool port = false;

	while ((opt = getopt(argc, argv, ":p:n:")) != -1) {
		switch (opt) {
		case 'p':
			strncat(endpoint, optarg,
				ARRAY_SIZE(endpoint) - strlen(endpoint));
			port = true;
			break;
		case 'n':
			filter[0] = optarg[0];
			break;
		default:
			fprintf(stderr, "Unknown argument\n");
			goto err;
		}
	}

	if (!port)
		strcat(endpoint, "5555");

	return 0;
err:
	zcli_print_usage(argv[0]);
	return -1;
}

int main(int argc, char **argv)
{
	ctx = zmq_ctx_new();
	socket = zmq_socket(ctx, ZMQ_SUB);

	int rc = zcli_parse_command_line(argc, argv);
	if (rc)
		zcli_exit(rc);

	rc = zmq_connect(socket, endpoint);
	assert(rc == 0);

	rc = zmq_setsockopt(socket, ZMQ_SUBSCRIBE, filter, strlen(filter));
	assert(rc == 0);

	signal(SIGINT, zcli_exit_sig);
	signal(SIGTERM, zcli_exit_sig);

	while (true) {
		char buf[256] = { '\0' };

		int size = zmq_recv(socket, buf, 255, 0);
		if (size < 0)
			zcli_exit(size);

		buf[size] = '\0';

		printf("%s\n", buf);
	}

	return 0;
}
