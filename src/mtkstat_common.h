#ifndef __MTKSTAT_COMMON_H
#define __MTKSTAT_COMMON_H

#define FILTERS_MAX 5

#include <stdbool.h>

struct filter_data {
	__u32 src_addr;
	__u32 dst_addr;
	__u16 src_port;
	__u16 dst_port;
	__u8  protocol;
	__u64 rx_pkts;
	__u64 rx_bytes;
	bool  enabled;
};

#endif /* __MTKSTAT_COMMON_H */
