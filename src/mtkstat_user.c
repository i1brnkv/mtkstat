#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <net/if.h>
#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <float.h>
#include <zmq.h>
#include "mtkstat_common.h"
#include "jsmn.h"

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

/* index of interface to which eBPF prog will be attached */
static unsigned int iface_idx;
/* eBPF prog flags */
static __u32 flags;
/* eBPF fds */
static struct _bpf_fds {
	int prog;
	int map;
} mtk_bpf_fds;
/* number of CPUs */
static int nr_cpus;
/* time of program start
 * will be needed to calculate speed*/
static unsigned long start_time;
/* file name with filters configurations */
static char *config_file;
/* period between measurements in ms */
static int period = 1000000;
/* struct to describe ZeroMQ socket */
struct _zmq {
	void *ctx;
	void *sock;
	char endpoint[32];
} mtk_zmq = {
	.endpoint = "tcp://*:"
};

static void print_usage(char *prog_name)
{
	printf("Usage: %s -i <interface> -f <config.json> [-t <ms>] [-p <ZeroMQ_port_number>]\n\n",
	       prog_name);
	printf("\t-i <interface>\tInterface on which eBPF program will run.\n"
	       "\t\t\tRequired.\n\n"
	       "\t-f <config>\tFilters configuration file in json format.\n"
	       "\t\t\tRequired.\n\n"
	       "\t-t <ms>\t\tPeriod between measurements in ms.\n"
	       "\t\t\tOptional, default is 1s.\n\n"
	       "\t-p <port_no>\tPort number to which statistics will be\n"
	       "\t\t\ttransmitted with ZeorMQ.\n"
	       "\t\t\tOptional, default is 5555.\n");
}

static int mtk_bpf_load(void)
{
	struct bpf_prog_load_attr prog_attr = {
		.file = "mtkstat_kern.o",
		.prog_type = BPF_PROG_TYPE_XDP };
	struct bpf_object *obj;
	struct bpf_map *map;
	int err;

	err = bpf_prog_load_xattr(&prog_attr, &obj, &mtk_bpf_fds.prog);
	if (err)
		return err;

	map = bpf_map__next(NULL, obj);
	if (!map) {
		fprintf(stderr, "Error! Failed to find map in eBPF object!\n");

		return -1;
	}

	mtk_bpf_fds.map = bpf_map__fd(map);

	return 0;
}

/* read config file into string
 * result need to be freed */
static char *mtk_config_read(const char *file)
{
	char *config_str = NULL;
	int fd = open(file, O_RDONLY);

	/* determine file size */
	off_t len = lseek(fd, 0, SEEK_END);
	if (len <= 0)
		return NULL;

	lseek(fd, 0, SEEK_SET);

	config_str = malloc(len + 1);
	if (!config_str)
		goto out;

	/* read file to string */
	ssize_t r_len = read(fd, config_str, len);
	if (r_len < len) {
		char *pos = config_str;
		while (r_len) {
			if (r_len < 0) {
				free(config_str);
				config_str = NULL;
				goto out;
			}

			len -= r_len;
			pos += r_len;
			r_len = read(fd, pos, len);
		}
	}

	config_str[len] = '\0';

out:
	close(fd);
	return config_str;
}

static void mtk_push_filter(int idx, struct filter_data filter)
{
	struct filter_data filters[nr_cpus];

	for (int i = 0; i < nr_cpus; i++)
		filters[i] = filter;

	bpf_map_update_elem(mtk_bpf_fds.map, &idx, &filters, BPF_ANY);
}

/* return number of filters in config */
static int mtk_parse_config(const char *file)
{
	int ret;
	char *config = mtk_config_read(file);
	if (!config) {
		ret = -1;
		goto out;
	}

	jsmn_parser parser;
	jsmntok_t tok[57]; /* 5 filters with 5 key/value pairs = 56 tokens */

	jsmn_init(&parser);
	int tok_cnt = jsmn_parse(&parser, config, strlen(config), tok,
				 ARRAY_SIZE(tok));
	if (tok_cnt < 0) {
		ret = tok_cnt;
		goto out;
	}

	/* top-level element should be array */
	if (tok[0].type != JSMN_ARRAY) {
		ret = -1;
		goto out;
	}
	if (tok[0].size > FILTERS_MAX) {
		ret = -1;
		goto out;
	}

	/* number of members in array
	 * return this if succeed */
	ret = tok[0].size;

	struct filter_data filter;
	int filter_param_cnt;
	int filter_param_len;
	int n_filter = 0;

	for (int i = 1; i < (tok_cnt - 1); i++) {
		/* start parsing new filter parameters */
		if (tok[i].type == JSMN_OBJECT) {
			filter = (struct filter_data){ 0 };
			filter.enabled = true;
			filter_param_cnt = tok[i].size;
			continue;
		} else if (tok[i].type == JSMN_STRING)
			filter_param_len = tok[i].end - tok[i].start;
		else {
			ret = -1;
			goto out;
		}

		char *val_str = strndup(config + tok[i+1].start,
					tok[i+1].end - tok[i+1].start);

		/* actual parameters parsing */
		if (strncmp(config + tok[i].start,
			    "src addr", filter_param_len) == 0) {
			struct in_addr bin_addr;

			if (!inet_aton(val_str, &bin_addr)) {
				free(val_str);
				ret = -1;
				goto out;
			}

			filter.src_addr = bin_addr.s_addr;
		} else if (strncmp(config + tok[i].start,
				   "dst addr", filter_param_len) == 0) {
			struct in_addr bin_addr;

			if (!inet_aton(val_str, &bin_addr)) {
				free(val_str);
				ret = -1;
				goto out;
			}

			filter.dst_addr = bin_addr.s_addr;
		} else if (strncmp(config + tok[i].start,
				   "src port", filter_param_len) == 0) {
			char *endptr = NULL;

			int port = strtol(val_str, &endptr, 10);
			if (*endptr != '\0' || port < 0) {
				free(val_str);
				ret = -1;
				goto out;
			}

			filter.src_port = htons(port);
		} else if (strncmp(config + tok[i].start,
				   "dst port", filter_param_len) == 0) {
			char *endptr = NULL;

			int port = strtol(val_str, &endptr, 10);
			if (*endptr != '\0' || port < 0) {
				free(val_str);
				ret = -1;
				goto out;
			}

			filter.dst_port = htons(port);
		} else if (strncmp(config + tok[i].start,
				   "protocol", filter_param_len) == 0) {
			if (strcmp(val_str, "TCP") == 0)
				filter.protocol = IPPROTO_TCP;
			else if (strcmp(val_str, "UDP") == 0)
				filter.protocol = IPPROTO_UDP;
			else {
				free(val_str);
				ret = -1;
				goto out;
			}
		} else {
			free(val_str);
			continue;
		}

		free(val_str);

		filter_param_cnt--;
		/* all filter parameters parsed,
		 * now push filter to the eBPF map */
		if (filter_param_cnt == 0) {
			mtk_push_filter(n_filter, filter);
			n_filter++;
		}
	}

out:
	free(config);
	return ret;
}

/* Avarage speed over program run time */
static float _stat_avg_speed(unsigned long bytes)
{
	struct timespec curr_ts;

	if (clock_gettime(CLOCK_MONOTONIC, &curr_ts)) {
		fprintf(stderr, "Failed to get time\n"
				"%s (%d)\n", strerror(errno), -errno);
		return -1;
	}

	unsigned long curr_time = curr_ts.tv_sec * 1000000000 + curr_ts.tv_nsec;

	float speed = (bytes * 8000.0) / (curr_time - start_time);

	return speed;
}

/* Instatn speed (avarage over 1 sec)
 * We need filter index here, because we need to store measurements
 * from previous call, and that storage is per filter */
static float _stat_inst_speed(int idx, unsigned long bytes)
{
	static unsigned long old_bytes[FILTERS_MAX];
	static unsigned long old_ns[FILTERS_MAX];
	static float speed[FILTERS_MAX];

	/* first run ever */
	if (old_ns[idx] == 0)
		old_ns[idx] = start_time;

	/* first second doesn't pass yet */
	if (old_ns[idx] == start_time)
		speed[idx] = _stat_avg_speed(bytes);

	struct timespec curr_ts;

	if (clock_gettime(CLOCK_MONOTONIC, &curr_ts)) {
		fprintf(stderr, "Failed to get time\n"
				"%s (%d)\n", strerror(errno), -errno);
		return -1;
	}

	unsigned long curr_ns = curr_ts.tv_sec * 1000000000 + curr_ts.tv_nsec;

	if (curr_ns - old_ns[idx] >= 1000000000) {
		speed[idx] = (bytes - old_bytes[idx]) * 8000.0 /
			     (curr_ns - old_ns[idx]);
		old_bytes[idx] = bytes;
		old_ns[idx] = curr_ns;
	}

	return speed[idx];
}

/* format pretty string with filters data
 * result string need to be freed */
static char *mtk_get_stats(int idx)
{
	struct filter_data filter[nr_cpus];
	char *str = NULL;

	/* FIXME: hardcoded initializer need to be fixed
	 * if FILTER_MAX define will be changed */
	static float min_speed[FILTERS_MAX] =
		{ FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX };
	static float max_speed[FILTERS_MAX];

	if (bpf_map_lookup_elem(mtk_bpf_fds.map, &idx, filter)) {
		fprintf(stderr, "Error! Failed to lookup filter %d\n", idx);
		goto err;
	}

	unsigned long rx_pkts = 0;
	unsigned long rx_bytes = 0;

	for (int i = 0; i < nr_cpus; i++) {
		rx_pkts  += filter[i].rx_pkts;
		rx_bytes += filter[i].rx_bytes;
	}

	float avg_spd = _stat_avg_speed(rx_bytes);
	float inst_spd = _stat_inst_speed(idx, rx_bytes);

	if (avg_spd < 0 || inst_spd < 0)
		goto err;

	if (inst_spd > max_speed[idx])
		max_speed[idx] = inst_spd;

	if (inst_spd < min_speed[idx])
		min_speed[idx] = inst_spd;

	struct in_addr bin_addr;
	char src_addr[INET_ADDRSTRLEN], dst_addr[INET_ADDRSTRLEN];

	bin_addr.s_addr = filter[0].src_addr;
	if (!inet_ntop(AF_INET, &bin_addr, src_addr, INET_ADDRSTRLEN)) {
		fprintf(stderr,
			"Failed to get src_addr from filter %d\n", idx);
		goto err;
	}

	bin_addr.s_addr = filter[0].dst_addr;
	if (!inet_ntop(AF_INET, &bin_addr, dst_addr, INET_ADDRSTRLEN)) {
		fprintf(stderr,
			"Failed to get dst_addr from filter %d\n", idx);
		goto err;
	}

	char src_port[6], dst_port[6];

	if (filter[0].src_port == 0)
		sprintf(src_port, "*");
	else
		snprintf(src_port, ARRAY_SIZE(src_port),
			 "%d", ntohs(filter[0].src_port));

	if (filter[0].dst_port == 0)
		sprintf(dst_port, "*");
	else
		snprintf(dst_port, ARRAY_SIZE(dst_port),
			 "%d", ntohs(filter[0].dst_port));

	char protocol[4];

	if (filter[0].protocol == IPPROTO_TCP)
		snprintf(protocol, ARRAY_SIZE(protocol), "TCP");
	else if (filter[0].protocol == IPPROTO_UDP)
		snprintf(protocol, ARRAY_SIZE(protocol), "UDP");
	else
		snprintf(protocol, ARRAY_SIZE(protocol), "ANY");

	int len = snprintf(NULL, 0,
			"%d: %s %s:%s -> %s:%s %lu/%lu %.3f/%.3f/%.3f",
			idx, protocol, src_addr, src_port, dst_addr, dst_port,
			rx_pkts, rx_bytes,
			min_speed[idx], avg_spd, max_speed[idx]);

	str = malloc(len + 1);

	snprintf(str, len + 1,
			"%d: %s %s:%s -> %s:%s %lu/%lu %.3f/%.3f/%.3f",
			idx, protocol, src_addr, src_port, dst_addr, dst_port,
			rx_pkts, rx_bytes,
			min_speed[idx], avg_spd, max_speed[idx]);

	return str;
err:
	free(str);
	return NULL;
}

static void mtk_print_stats(int idx, char *stat_string)
{
	int i;

	for (i = 0; i < idx; i++)
		printf("\n");

	printf("\33[2K");	/* clear current line */

	printf("%s", stat_string);

	/* return cursor to the beginning */
	printf("\r");
	for (i = 0; i < idx; i++)
		printf("\033[A"); /* move one line up */
}

static int mtk_zmq_init(void)
{
	mtk_zmq.ctx = zmq_ctx_new();
	mtk_zmq.sock = zmq_socket(mtk_zmq.ctx, ZMQ_PUB);

	return zmq_bind(mtk_zmq.sock, mtk_zmq.endpoint);
}

static void mtk_zmq_fini(void)
{
	zmq_close(mtk_zmq.sock);
	zmq_ctx_destroy(mtk_zmq.ctx);
}

static void mtk_send_stats(char *stat_string)
{
	zmq_send(mtk_zmq.sock, stat_string, strlen(stat_string), 0);
}

static int mtk_parse_command_line(int argc, char **argv)
{
	if (argc < 5) {
		print_usage(argv[0]);
		return -1;
	}

	int opt;
	char *endptr = NULL;
	bool port = false;

	while ((opt = getopt(argc, argv, ":i:f:t:p:")) != -1) {
		switch (opt) {
		case 'i':
			iface_idx = if_nametoindex(optarg);
			break;
		case 'f':
			config_file = strdup(optarg);
			break;
		case 't':
			period = strtol(optarg, &endptr, 10);
			if (*endptr != '\0' || period < 0)
				goto err;

			period *= 1000; /* ms to us, becouse it will
					 * be used in usleep() */
			break;
		case 'p':
			strncat(mtk_zmq.endpoint, optarg,
				ARRAY_SIZE(mtk_zmq.endpoint) -
				strlen(mtk_zmq.endpoint));
			port = true;
			break;
		default:
			fprintf(stderr, "Unknown argument\n");
			goto err;
		}
	}

	if (iface_idx == 0 || config_file == NULL)
		goto err;

	if (!port)
		strcat(mtk_zmq.endpoint, "5555");
	return 0;

err:
	free(config_file);
	print_usage(argv[0]);
	return -1;
}

static void mtk_exit(int status)
{
	mtk_zmq_fini();
	bpf_set_link_xdp_fd(iface_idx, -1, flags);
	exit(status);
}

static void _mtk_exit_sig(int sig)
{
	mtk_exit(0);
}

int main(int argc, char **argv)
{
	int err;
	struct timespec start;

	err = clock_gettime(CLOCK_MONOTONIC, &start);
	if (err) {
		fprintf(stderr, "Failed to get program start time\n"
				"%s (%d)\n", strerror(errno), -errno);
		return err;
	}
	start_time = start.tv_sec * 1000000000 + start.tv_nsec;

	nr_cpus = libbpf_num_possible_cpus();

	err = mtk_parse_command_line(argc, argv);
	if (err)
		exit(err);

	err = mtk_bpf_load();
	if (err)
		exit(err);

	int nr_filters = mtk_parse_config(config_file);
	if (nr_filters < 0) {
		fprintf(stderr, "Error! Failed to parse config\n");

		return nr_filters;
	}

	err = mtk_zmq_init();
	if (err) {
		fprintf(stderr, "Failed to init ZeroMQ\n");

		return err;
	}

	signal(SIGINT, _mtk_exit_sig);
	signal(SIGTERM, _mtk_exit_sig);

	err = bpf_set_link_xdp_fd(iface_idx, mtk_bpf_fds.prog, flags);
	if (err) {
		fprintf(stderr, "Error! Set link xdp fd failed\n");
		fprintf(stderr, "%s (%d)\n", strerror(-err), err);

		return err;
	}

	char *stat_msg = NULL;

	while (true) {
		usleep(period);
		for (int i = 0; i < nr_filters; i++) {
			stat_msg = mtk_get_stats(i);
			if (stat_msg) {
				mtk_send_stats(stat_msg);
				mtk_print_stats(i, stat_msg);
			} else {
				fprintf(stderr,
					"Failed to get filters statistics\n");
				free(stat_msg);
				mtk_exit(-1);
			}
			free(stat_msg);
		}
	}

	mtk_exit(0);
}
