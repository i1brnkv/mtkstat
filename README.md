# eBPF XDP example

Analyze incoming packets and count them if they match specific filters.

## How to build

Clone it recursevely, because it contains submodules

```
git clone --recurse-submodules https://gitlab.com/i1brnkv/mtkstat.git
```

### Dependencies:
 * Linux kernel 4.18 and newer
 * linux-headers
 * clang, llvm 4.0 and newer
 * libelf
 * libzmq
 * pkg-config

On debian-10 (current stable) run:
```
sudo apt-get install -y linux-headers-`uname -r` \
                        clang \
                        llvm \
                        libelf-dev \
                        libzmq3-dev \
                        pkg-config
```

### Build:
```
cd src
make
```

Result:

|||
| --- | --- |
| `mtkstat_kern.o` | eBPF object |
| `mtkstat` | main user-space program |
| `zmq_client` | program to read ZeroMQ messages from main program |

## mtkstat_kern.c

eBPF program. It parses packets headers, compare with filter
 * source IP-address
 * source port
 * destination IP-addres
 * destination port
 * protocol TCP or UDP or any of these two

and increase packets and bytes counters for matching filter.

## mtkstat_user.c

User-space program
 * load eBPF programm, link it to specified network interface
 * load filters to eBPF program
 * read statistics
 * send statistics to ZeroMQ socket (as publisher in pub-sub model)
 * print statistics to stdout as well

### Filters description

`mtkstat` expects config file with filters description in JSON format,
like this:

```json
[
	{
		"src addr": "1.2.3.4",
		"src port": "5678",
		"dst addr": "5.6.7.8",
		"dst port": "99",
		"protocol": "ANY"
	},
	{
		"dst addr": "8.8.8.8",
		"protocol": "TCP"
	}
]
```

Key-values should be all strings. 

If you want address or port to be wildcard, just omit it. For example,
in second filter `"src addr"` and `"src port"` would be wildcard (`0.0.0.0:*`)

### Command line arguments

||||
| --- | --- | --- |
| `-i <iface>` | specify interface to which eBPF program will be linked | required argument |
| `-f <file.json>` | specify file with filters config file | required argument |
| `-t <ms>` | specify period between measurements in milli-seconds | optional argument. Default value is 1 second |
| `-p <port_no>` | specify port number to which statistics will be transmitted with ZeorMQ | optional argument, default is 5555 |

### Example

```
$ cat filters.json
```

```json
[
	{
		"protocol": "UDP"
	},
	{
		"protocol": "TCP"
	}
]
```

```
$ sudo ./mtkstat -i wlp1s0 -f filters.json -t 500
```

```
0: UDP 0.0.0.0:* -> 0.0.0.0:* 37/6355 0.003/0.006/0.012
1: TCP 0.0.0.0:* -> 0.0.0.0:* 12423/882069 0.181/0.881/1.397
```
Output format:

| filter index | protocol | src addr | src port | dst addr | dst port | packets count | bytes count | min speed | avg speed | max speed |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 0 | UDP | 0.0.0.0 | * | 0.0.0.0 | * | 37 | 6355 | 0.003 | 0.006 | 0.012 |
| 1 | TCP | 0.0.0.0 | * | 0.0.0.0 | * | 12423 | 882069 | 0.181 | 0.881 | 1.397 |

Speed is in Mbits/sec

## zmq_client.c

Test utility to receive ZeroMQ messages from `mtkstat`

### Command line arguments

||||
| --- | --- | --- |
| `-p <port_no>` | specify port number from which ZeroMQ messages will be received | optional argument, default value is 5555 |
| `-n <filter_idx>` | receive statistics only for specified filter | optional argument, if not set - print all |
